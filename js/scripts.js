var fieldsBlockHeight;
if($('.block_collapsed').length){
	fieldsBlockHeight = $('.block_collapsed').outerHeight();
	$('.block_collapsed').css('height',0);
}

$(document).ready(function(){
	$('.bannerSee').click(function(e){
		e.preventDefault();
		var thisBtn = $(this);
		if(!thisBtn.parents('.caption').hasClass('large')) {
			$('.bannerSee').html('See more');
			$('.thumbnail .caption').removeClass('large');
			$('.thumbnail .caption .bannerType').removeClass('whitebg');
			setTimeout(function(){
				thisBtn.html('See less');
				thisBtn.parents('.caption').addClass('large');
				thisBtn.parents('.bannerButtons').siblings('.bannerType').addClass('whitebg');
			}, 100);
		} else {
			thisBtn.html('See more');
			thisBtn.parents('.caption').removeClass('large');
			thisBtn.parents('.bannerButtons').siblings('.bannerType').removeClass('whitebg');
		}
		setTimeout(function(){
			//console.log($('.pageMain').outerHeight(true));
			//$('.container .sidemenu .list-group').css({'height': $('.pageMain').outerHeight(true)});
		}, 200);
	});

	$('.resGrid').click(function() {
		var thisBlock = $(this);
		$('.resGrid').removeClass('active');
		thisBlock.addClass('active');
		$('body').css('height','auto');
		setTimeout(function(){
			$('.resultsTable .modal').removeClass('in').removeAttr('style');
		}, 150);
		$('.bannerGen').html('Generate Code');
		if(thisBlock.hasClass('resultsList')) {
			$('body').removeClass('tile').addClass('list');
			$('.resultsTable').masonry('destroy')
		} else {
			$('body').removeClass('list').addClass('tile');
			theTileGrid();
		}
	});

	$('.close').click(function() {
		$('.bannerGen').html('Generate Code');
		$('body').css('height','auto');
		setTimeout(function(){
			$('.resultsTable .modal').removeClass('in').removeAttr('style');
		}, 150);
	});
	
	
	$('.click-more-options').click(function() {
		thisBtn = $(this);
		if(thisBtn.hasClass('opened')) {
			thisBtn.removeClass('opened');
			$('.block_collapsed').animate({'height':0},300);
			$('.click-more-options').html('More Filters');
		} else {
			thisBtn.addClass('opened');
			$('.block_collapsed').animate({'height':fieldsBlockHeight},300);
			$('.click-more-options').html('Less Filters');
		}
		
	});

	$(".form-control-area").mCustomScrollbar({
		setHeight:150,
		theme:"minimal-dark"
	});
	$(".statistic-inner-table").mCustomScrollbar({
		axis:"x",
		theme:"3d-thick",
		scrollButtons:{enable:true},
		advanced:{autoExpandHorizontalScroll:true}
	});

	$(".finance-inner-table").mCustomScrollbar({
		axis:"x",
		theme:"3d-thick",
		scrollButtons:{enable:true},
		advanced:{autoExpandHorizontalScroll:true}
	});

		if($(window).width()<1024) {

			$(".finance-inner-table").mCustomScrollbar({
				axis:"x",
				theme:"3d-thick",
				scrollButtons:{enable:true},
				advanced:{autoExpandHorizontalScroll:true}
			});

		}
	if($(window).width()<1024) {

		$(".paymentacc-inner-table").mCustomScrollbar({
			axis:"x",
			theme:"3d-thick",
			scrollButtons:{enable:true},
			advanced:{autoExpandHorizontalScroll:true}
		});

	}




	
	//Remove website button on Account Detail page
	$( '.siteURLs' ).on( 'click', '.deleteBtn', function() {
		if($('.siteURLs .surl_wrapper').length > 1){
			$(this).parents('.surl_wrapper').remove();
		}
	});
	
	//Add website button on Account Detail page
	$('.addWebsite').click(function() {
		$('.siteURLs').find('.surl_wrapper:first').clone().appendTo('.siteURLs');
		$('.siteURLs').find('.surl_wrapper:last').find('input').val('');
	});

	$('.bannerGen').click(function(){
		var thisBtn = $(this);
		var thisModalID = $(this).attr('data-target');
		setTimeout(function(){
			$('.resultsTable .modal').each(function(){

				if($(this).attr('id') == thisModalID.substring(1, thisModalID.length)) {$(this).css({'display':'block', 'padding':0});} else {$(this).removeAttr('style');}
			});
		}, 350);
		if($(thisModalID).hasClass('in')) {
			thisBtn.html('Generate Code');
			$('body').css('height','auto');
		} else {

			$('body').css('height','auto');

			$('.bannerGen').html('Generate Code');
			thisBtn.html('Hide Code');

			setTimeout(function(){
				$('.resultsTable .modal').each(function(){
					if($(this).attr('id') !== thisModalID.substring(1, thisModalID.length)) {$(this).removeClass('in'); } else {$(this).addClass('in');}
				});
				if($('body').hasClass('tile')){
					if($('body').width() > 640){
						$(thisModalID).animate({'top': (thisBtn.offset().top - thisBtn.parents('.thumbnail').offset().top - thisBtn.parents('.thumbnail').scrollTop()) + 30 }, 300);
						if($('body').height() < ($(thisModalID).offset().top + $(thisModalID).height() + 50) ) {
							$('body').css('height', ($(thisModalID).offset().top + $(thisModalID).height() + 50));
						}
					} else {
						$(thisModalID).animate({'top': (thisBtn.offset().top - thisBtn.parents('.thumbnail').offset().top - thisBtn.parents('.thumbnail').scrollTop())  - thisBtn.parents('.captInner').height() + 73 }, 300);
						if($('body').height() < ($(thisModalID).offset().top + $(thisModalID).height() + 50) ) {
							$('body').css('height', ($(thisModalID).offset().top + $(thisModalID).height() + 50));
						}
					}
				}
			}, 150);


		}
	});

	$('.bannerSee').click(function(){
		setTimeout(function(){
			theTileGrid();
		},150);

	});
	
	
	//$('.container .sidemenu .list-group').css({'height': $('.pageMain').outerHeight(true)});
});


$( window ).load( function(){
	theTileGrid();
	dashboardResize();
});

$(window).on('resize', function(){
	//$('.container .sidemenu .list-group').css({'height': $('.pageMain').outerHeight(true)});
	dashboardResize();
});


function theTileGrid(){
    var columns    = 4,
    setColumns = function() { columns = $( window ).width() > 640 ? 4 : $( window ).width() > 320 ? 2 : 1; };

    setColumns();
    $( window ).resize( setColumns );

    $('.resultsTable').masonry(
    {
        itemSelector: '.resultsTable .col-sm-3.col-lg-3.col-md-3',
        columnWidth:  function( containerWidth ) { return containerWidth / columns; }
    });
}

function dashboardResize(){
	if($('.row.dashboard').length){
		var isVisible = $('#separateConv').is(':hidden');
		console.log(isVisible);
		if(isVisible){
			//$('.dashboard_wrapper.graph .dashboard_inner').css('height', ($('.dashboard_right_block.with-conversion').height() + parseInt($('.dashboard_right_block.with-conversion').css('margin-top'), 10) -4));
		} else {
			//$('.dashboard_wrapper.graph .dashboard_inner').css('height', 400);
		}

	}
}
/////////////mobile-menu
$(".offc-button").click(function(){
	$('.sidemenu').slideToggle(450);
});
///////////////////////

$(".x").click(function(){
	$(".box-pass").hide();
	$(".title-account-text").show()
});